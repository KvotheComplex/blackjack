from classes import deck, hand, chip, turn_counter

name_entered = False
playing = True

player_balance = chip.Chips()
counter = turn_counter.TurnCounter()

new_deck = deck.Deck()
new_deck.shuffle_cards()

dealer = hand.Hand("Dealer")

def take_bet(bet):
    while bet > player_balance.total:
        print(f"Insufficent funds remaining. Your current balance is {player_balance.total}")
        while True: 
            bet = input("Please enter your bet\n")
            try: 
                bet = int(bet)
                break
            except ValueError:
                print("Invalid response, Please enter an amount to bet")

    player_balance.bet = bet
    print('\n')
    print("Your bet has been accepted. Good luck!")

def new_round():
    counter.next_turn()
    player1.cards = []
    player1.value = 0
    dealer.cards = []
    dealer.value = 0
    print(f"Your balance is {player_balance.total}")

while True:
    if not name_entered:
        player1 = hand.Hand(input("Please enter your name \n"))
        print(f'So your name is {player1.name}?')
        answer = input("Please type yes or no \n").lower()
        while answer not in ['yes', 'y', 'no', 'n']:
            print("Invalid input, please type yes or no")
            answer = input("").lower()
        if answer in ['yes', 'y']:
            print(f'Ok {player1.name}, welcome to the table!')
            break
        else:
            print('Ok, lets try again')
            name_entered = False

while playing:
    new_round()
    #Take bet
    if player_balance.total < 5:
        print("You do not have enough funds remaining to meet the minimum ante")
        print("GAME OVER")
        exit()
    else:
        while True: 
            bet = input("Please enter your bet\n")
            try: 
                bet = int(bet)
                break
            except ValueError:
                print("Invalid response, Please enter an amount to bet")
         
        take_bet(bet)
    print('\n')
    print("Dealer: I will now deal the cards")
    print("\n")
    card1 = new_deck.deal()
    player1.add_cards(card1)
    card2 = new_deck.deal()
    player1.add_cards(card2)
    print(f"Your current cards are the {player1.cards[0]} and the {player1.cards[1]}")
    print('\n')
    print(f"Your current total is {player1.value}")

    while True:
        response = input("Would you like another card, please type yes or no \n").lower()
        while response not in ["yes", "y", "no", "n"]:
            response = input("Invalid response, please type yes or no \n").lower()
        if response in ['yes', 'y']:
            new_card = new_deck.deal()
            player1.add_cards(new_card)
            player1.adjust_for_aces()
            print(f"Your new card was the {new_card}")
            print(f'Your new total is {player1.value}')
            if player1.value > 21:
                print("BUST!!!")
                player_balance.lose_bet()
                break
        else:
            print(f"OK, lets see if I can beat {player1.value}")

            dealer_card1 = new_deck.deal()
            dealer.add_cards(dealer_card1)
            dealer_card2 = new_deck.deal()
            dealer.add_cards(dealer_card2)

            print(f'The dealer draws the {dealer.cards[0]} and the {dealer.cards[1]} for a total of {dealer.value}')
            while dealer.value <=16:
                print("I will take another card")
                print('\n')
                new_dealer_card = new_deck.deal()
                dealer.add_cards(new_dealer_card)
                dealer.adjust_for_aces()
                print(f'The dealer draws the {new_dealer_card} for a new total of {dealer.value}')
            if dealer.value >= 17 and dealer.value <= 21:
                print("I will stand")
                if dealer.value > player1.value:
                    print("Looks like I win... Better luck on the next hand")
                    player_balance.lose_bet()
                    break
                elif dealer.value == player1.value:
                    print("PUSH!")
                    break
                else:
                    print("Looks like you beat me this time. Congrats!")
                    player_balance.win_bet()
                    break
            elif dealer.value > 21:
                print("The dealer bust!!!")
                player_balance.win_bet()
                break
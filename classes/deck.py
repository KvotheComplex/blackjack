#Deck Class

from classes import card
import random

""" 
Should instaniate a new deck of all 52 card objects
hold a list of those objects
shuffle the deck and deal cards from the deck
use pop to remove cards from the list
"""

class Deck:
    def __init__(self):
        self.cards = []

        for suit in card.suits: 
            for rank in card.ranks:
                new_card = card.Card(suit, rank)
                self.cards.append(new_card)

    def __str__(self):
        deck_comp = ''
        for card in self.cards:
            deck_comp += '\n' + card.__str__()
        return "The deck has: " + deck_comp

    def shuffle_cards(self):
        random.shuffle(self.cards)

    def deal(self):
        return self.cards.pop()
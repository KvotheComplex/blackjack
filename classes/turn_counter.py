class TurnCounter:
    def __init__(self):
        self.turn = 0

    def next_turn(self):
        self.turn += 1
        print(f"This is hand {self.turn}")

#Hand Class
from classes import card, deck

class Hand():
    def __init__(self, name):
        self.name = name
        self.cards = []
        self.value = 0
        self.aces = 0

    def add_cards(self, card):
        self.cards.append(card)
        self.value += card.value

        if card.rank == "Ace":
            self.aces += 1

    def adjust_for_aces(self):
        while self.value > 21 and self.aces:
            self.value -= 10
            self.aces -=1

#Player Class

class Player:
    def __init__(self,name):
        self.name = name
        self.hand = []

    def remove_card_from_hand(self):
        return self.hand.pop(0)

    def add_card_to_hand(self,new_cards):
        if type(new_cards) == type([]):
            self.hand.extend(new_cards)
        else:
            self.hand.append(new_cards)

    def __str__(self):
        return f'{self.name} has {len(self.hand)} cards.'